package group.MicroService.Controller;

import group.MicroService.Models.Commande;
import group.MicroService.Repository.CommandeRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/commande")
public class CommandeController {

    @Autowired
    private CommandeRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Commande> getAllCommande() {
        return repository.findAll();
    }
    @RequestMapping(value = "/", params = "idClient", method = RequestMethod.GET)
    public List<Commande> getCommandeByIdClient(@RequestParam(value = "idClient") ObjectId idClient) {
        return repository.findCommandeByIdClient(idClient);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Commande getCommandeById(@PathVariable("id") ObjectId id) {
        return repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyCommandeById(@PathVariable("id") ObjectId id, @Valid @RequestBody Commande commande) {
        commande.set_id(id);
        repository.save(commande);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Commande createCommande(@Valid @RequestBody Commande commande) {
        commande.set_id(ObjectId.get());
        repository.save(commande);
        return commande;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteCommande(@PathVariable ObjectId id) {
        repository.delete(repository.findBy_id(id));
    }

}
