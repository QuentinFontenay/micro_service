package group.MicroService.Controller;

import group.MicroService.Models.Equipement;
import group.MicroService.Repository.EquipementRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController 
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/equipement")
public class EquipementController {

    @Autowired
    private EquipementRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Equipement> getAllEquipement() {
        return repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Equipement getEquipementById(@PathVariable("id") ObjectId id) {
        return repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyEquipementById(@PathVariable("id") ObjectId id, @Valid @RequestBody Equipement equipement) {
        equipement.set_id(id);
        repository.save(equipement);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Equipement createEquipement(@Valid @RequestBody Equipement equipement) {
        equipement.set_id(ObjectId.get());
        repository.save(equipement);
        return equipement;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteEquipement(@PathVariable ObjectId id) {
        repository.delete(repository.findBy_id(id));
    }

}
