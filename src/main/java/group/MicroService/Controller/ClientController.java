package group.MicroService.Controller;

import group.MicroService.Models.Client;
import group.MicroService.Repository.ClientRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Client> getAllClient() {
        return repository.findAll();
    }

    @RequestMapping(value = "/", params = "idUser", method = RequestMethod.GET)
    public List<Client> getClientByIdUser(@RequestParam(value = "idUser") String idUser) {
        return repository.findClientByIdUser(idUser);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Client getClientById(@PathVariable("id") ObjectId id) {
        return repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyClientById(@PathVariable("id") ObjectId id, @Valid @RequestBody Client client) {
        client.set_id(id);
        repository.save(client);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Client createClient(@Valid @RequestBody Client client) {
        repository.save(client);
        return client;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteClient(@PathVariable ObjectId id) {
        repository.delete(repository.findBy_id(id));
    }

}
