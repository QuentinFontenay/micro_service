package group.MicroService.Repository;

import group.MicroService.Models.Commande;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CommandeRepository extends MongoRepository<Commande, String> {

    Commande findBy_id(ObjectId _id);

    List<Commande> findCommandeByIdClient(ObjectId idClient);
}
