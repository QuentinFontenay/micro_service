package group.MicroService.Repository;

import group.MicroService.Models.Client;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ClientRepository extends MongoRepository<Client, String> {

    Client findBy_id(ObjectId _id);

    List<Client> findClientByIdUser(String idUser);
}
