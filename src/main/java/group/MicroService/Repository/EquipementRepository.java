package group.MicroService.Repository;

import group.MicroService.Models.Equipement;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EquipementRepository extends MongoRepository<Equipement, String> {

    Equipement findBy_id(ObjectId _id);
}
