package group.MicroService.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Commande {

    @Id
    public ObjectId _id;

    public Date dateCommande;
    public ObjectId idClient;
    public ObjectId idEquipement;
    public Integer nbJourReserver;
    public float prixCommande;

    public Commande(ObjectId _id, Date dateCommande, ObjectId idClient, ObjectId idEquipement, Integer nbJourReserver, float prixCommande){
        this._id = _id;
        this.dateCommande = dateCommande;
        this.idClient = idClient;
        this.idEquipement = idEquipement;
        this.nbJourReserver = nbJourReserver;
        this.prixCommande = prixCommande;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getIdClient() {
        return idClient.toHexString();
    }

    public void setIdClient(ObjectId idClient) {
        this.idClient = idClient;
    }

    public String getIdEquipement() {
        return idEquipement.toHexString();
    }

    public void setIdEquipement(ObjectId idEquipement) {
        this.idEquipement = idEquipement;
    }

    public Integer getNbJourReserver() {
        return nbJourReserver;
    }

    public void setNbJourReserver(Integer nbJourReserver) {
        this.nbJourReserver = nbJourReserver;
    }

    public float getPrixCommande() {
        return prixCommande;
    }

    public void setPrixCommande(float prixCommande) {
        this.prixCommande = prixCommande;
    }
}
