package group.MicroService.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Client {

    @Id
    public ObjectId _id;

    public String name;
    public String email;
    public String adresse;
    public String telephone;
    public String idUser;

    public Client(ObjectId _id, String name, String email, String adresse, String telephone, String idUser) {
        this._id = _id;
        this.name = name;
        this.email = email;
        this.adresse = adresse;
        this.telephone = telephone;
        this.idUser = idUser;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
