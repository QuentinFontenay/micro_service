package group.MicroService.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Equipement {

    @Id
    public ObjectId _id;

    public String name;
    public String sport;
    public Integer prix;
    public Integer quantite;
    public String description;
    public String image;

    public Equipement(ObjectId _id, String name, String sport, Integer prix, Integer quantite, String description, String image) {
        this._id = _id;
        this.name = name;
        this.sport = sport;
        this.quantite = quantite;
        this.prix = prix;
        this.description = description;
        this.image = image;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
